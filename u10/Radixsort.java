
public class Radixsort {
	public static int[] radixSort(int[] array) {
		if (array.length > 1) {
			int max = max(array);
			for (int i = 1; max/i > 0; i*=10) {
				countingSort(array, i);
			}
		}
		return array;
	}
	
	public static void countingSort(int[] array, int stelle) {
		int[] anzahl = new int[10];
		for (int i = 0; i < array.length; i++) {
			anzahl[(array[i]/stelle)%10]++;
		}
		
		for (int i = 1; i < anzahl.length; i++) {
			anzahl[i] += anzahl[i-1];
		}
		
		int[] neuerArray = new int[array.length];
		for (int i = array.length-1; i >= 0; i--) {
			anzahl[(array[i]/stelle)%10]--;
			neuerArray[anzahl[(array[i]/stelle)%10]] = array[i];
		}
		
		for (int i = 0; i < array.length; i++) {
			array[i] = neuerArray[i];
		}
	}
	
	public static int max(int[] array) {
		int max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
}

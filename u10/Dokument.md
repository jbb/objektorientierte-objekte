# 10. Aufgabenblatt
## Aufgabe 1: ShellSort und InsertionSort
a)
```Python
from typing import List, Any
import copy
import random

MAGIC_LIST: List[int] = [1391376, 463792, 198768, 85961, 33936, 13776, 4592,
                         1968, 861, 336, 112, 48, 21, 7, 3, 1]


def jump_insert_sort(a: List[Any], start: int, step: int):
    for i in range(start + step, len(a), step):
        value: Any = a[i]
        j = i
        while j >= step and a[j - step] > value:
            a[j] = a[j - step]
            j = j - step
        a[j] = value


def shell_sort(a: List[Any]):
    magic_list_i: int = 0

    for n in MAGIC_LIST:
        if n < len(a):
            magic_list_i = MAGIC_LIST.index(n)
            break

    while magic_list_i < len(MAGIC_LIST):
        for start_i in range(MAGIC_LIST[magic_list_i]):
            jump_insert_sort(a, start_i, MAGIC_LIST[magic_list_i])

        magic_list_i += 1
```

b)
```Python
from typing import List, Any
import copy
import random

MAGIC_LIST: List[int] = [1391376, 463792, 198768, 85961, 33936, 13776, 4592,
                         1968, 861, 336, 112, 48, 21, 7, 3, 1]


def jump_insert_sort_count(a: List[Any], start: int, step: int) -> int:
    comparisons = 1

    for i in range(start + step, len(a), step):
        value: Any = a[i]
        j = i
        while j >= step and a[j - step] > value:
            comparisons += 1
            a[j] = a[j - step]
            j = j - step
        a[j] = value

    return comparisons

def shell_sort_count(a: List[Any]):
    comparisons = 0
    magic_list_i: int = 0

    for n in MAGIC_LIST:
        if n < len(a):
            magic_list_i = MAGIC_LIST.index(n)
            break

    while magic_list_i < len(MAGIC_LIST):
        for start_i in range(MAGIC_LIST[magic_list_i]):
            comparisons += jump_insert_sort_count(a, start_i, MAGIC_LIST[magic_list_i])

        magic_list_i += 1

    return comparisons
```

c) 
```Python
from typing import List, Any
import copy
import random

MAGIC_LIST: List[int] = [1391376, 463792, 198768, 85961, 33936, 13776, 4592,
                         1968, 861, 336, 112, 48, 21, 7, 3, 1]


def jump_insert_sort_count(a: List[Any], start: int, step: int) -> int:
    comparisons = 1

    for i in range(start + step, len(a), step):
        value: Any = a[i]
        j = i
        while j >= step and a[j - step] > value:
            comparisons += 1
            a[j] = a[j - step]
            j = j - step
        a[j] = value

    return comparisons


def shell_sort_count(a: List[Any]):
    comparisons = 0
    magic_list_i: int = 0

    for n in MAGIC_LIST:
        if n < len(a):
            magic_list_i = MAGIC_LIST.index(n)
            break

    while magic_list_i < len(MAGIC_LIST):
        for start_i in range(MAGIC_LIST[magic_list_i]):
            comparisons += jump_insert_sort_count(a, start_i, MAGIC_LIST[magic_list_i])

        magic_list_i += 1

    return comparisons


def insertion_sort_count(a: List[Any]):
    i = 1

    comparisons = 0

    while i < len(a):
        j = i

        comparisons += 1

        while j > 0 and a[j - 1] > a[j]:
            comparisons += 1
            a[j], a[j - 1] = a[j - 1], a[j]
            j = j - 1

        i = i + 1

    return comparisons


def table(shell: int, insert: int):
    print("Algorithm\tShellsort\tInsertion Sort")
    print(f"Comparisons\t{shell}\t\t{insert}")


if __name__ == "__main__":
    print("Random:")
    a = [random.randint(1, 10000) for i in range(1, 10000)]
    comparisons_shell_sort = shell_sort_count(copy.deepcopy(a))
    comparisons_insertion_sort = insertion_sort_count(a)
    table(comparisons_shell_sort, comparisons_insertion_sort)

    print("Sorted:")
    a = list(range(1, 10000))
    comparisons_shell_sort = shell_sort_count(copy.deepcopy(a))
    comparisons_insertion_sort = insertion_sort_count(a)
    table(comparisons_shell_sort, comparisons_insertion_sort)

    print("Reverse Sorted:")
    a = list(range(10000, 1, -1))
    comparisons_shell_sort = shell_sort_count(copy.deepcopy(a))
    comparisons_insertion_sort = insertion_sort_count(a)
    table(comparisons_shell_sort, comparisons_insertion_sort)
```

## Aufgabe 2: HeapSort-Algorithmus
```Python
# Alphabetisch sortiert
[(1, 'a'), (1, 'b'), (5, 'c'), (3, 'd'), (4, 'e'), (4, 'f')]

# max_heapify nach Zahlen:
[(5, 'c'), (1, 'b'), (1, 'a'), (3, 'd'), (4, 'e'), (4, 'f')]
[(5, 'c'), (1, 'b'), (4, 'f'), (3, 'd'), (4, 'e'), (1, 'a')]
[(5, 'c'), (3, 'd'), (4, 'f'), (1, 'b'), (4, 'e'), (1, 'a')]
[(5, 'c'), (4, 'e'), (4, 'f'), (1, 'b'), (3, 'd'), (1, 'a')]

# Sortieren
[(4, 'e'), (4, 'f'), (1, 'b'), (3, 'd'), (1, 'a')]
[(4, 'f'), (1, 'b'), (3, 'd'), (1, 'a')]
# Heap eigenschaften reparieren
[(3, 'd'), (1, 'b'), (1, 'a')]
[(1, 'b'), (1, 'a')]
[]

# Ergebnis
[(5, 'c'), (4, 'e'), (4, 'f'), (3, 'd'), (1, 'b'), (1, 'a')]

```

b ist vor a, stabilität verletzt

## Aufgabe 3: Effizient Sortieren
Radixsort ist für eine hohe Anzahl an zusortierenden Elementen gut geeignet, da es einen linearen Aufwand O(n) hat.
```Java
public class Radixsort {
	public static int[] radixSort(int[] array) {
		if (array.length > 1) {
			int max = max(array);
			for (int i = 1; max/i > 0; i*=10) {
				countingSort(array, i);
			}
		}
		return array;
	}
	
	public static void countingSort(int[] array, int stelle) {
		int[] anzahl = new int[10];
		for (int i = 0; i < array.length; i++) {
			anzahl[(array[i]/stelle)%10]++;
		}
		
		for (int i = 1; i < anzahl.length; i++) {
			anzahl[i] += anzahl[i-1];
		}
		
		int[] neuerArray = new int[array.length];
		for (int i = array.length-1; i >= 0; i--) {
			anzahl[(array[i]/stelle)%10]--;
			neuerArray[anzahl[(array[i]/stelle)%10]] = array[i];
		}
		
		for (int i = 0; i < array.length; i++) {
			array[i] = neuerArray[i];
		}
	}
	
	public static int max(int[] array) {
		int max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
}
```

|Vorteile	|Nachteile		|
|---		|---			|
|schnell	|out of place	|
|stabil		|				|

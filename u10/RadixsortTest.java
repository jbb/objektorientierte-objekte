import java.util.concurrent.ThreadLocalRandom;

public class RadixsortTest {
	public static void main(String[] args) {
		int[] array = new int[1_000_000];
		fillIntArrayWithRandomNumbers(array);
		
		long startTime = System.currentTimeMillis();
		Radixsort.radixSort(array);
		long endTime = System.currentTimeMillis();
		System.out.println("Zeit in ms: " + (endTime-startTime));
		
		//for (int i = 0; i < array.length; i++) {
		//	System.out.println(array[i]);
		//}
	}
	
	public static void fillIntArrayWithRandomNumbers(int[] array) {
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(Integer.MAX_VALUE);
		}
	}
}

import java.util.Scanner;

public class Caesar {
	
	final static char[] ALPHABET = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("  ██████╗ █████╗ ███████╗███████╗ █████╗ ██████╗ ██╗  ██╗ ██████╗ ██████╗ ██╗███████╗██████╗ ██╗   ██╗███╗   ██╗ ██████╗ ");
		System.out.println(" ██╔════╝██╔══██╗██╔════╝██╔════╝██╔══██╗██╔══██╗██║ ██╔╝██╔═══██╗██╔══██╗██║██╔════╝██╔══██╗██║   ██║████╗  ██║██╔════╝ ");
		System.out.println(" ██║     ███████║█████╗  ███████╗███████║██████╔╝█████╔╝ ██║   ██║██║  ██║██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║██║  ███╗");
		System.out.println(" ██║     ██╔══██║██╔══╝  ╚════██║██╔══██║██╔══██╗██╔═██╗ ██║   ██║██║  ██║██║██╔══╝  ██╔══██╗██║   ██║██║╚██╗██║██║   ██║");
		System.out.println(" ╚██████╗██║  ██║███████╗███████║██║  ██║██║  ██║██║  ██╗╚██████╔╝██████╔╝██║███████╗██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝");
		System.out.println("  ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚═╝╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ");
		while (true) {
			System.out.println("-------------------------------------------------------------------------------------------------------------------------");
			System.out.print("Text eingeben: ");
			String text = input.nextLine();
			System.out.print("a=");
			int shiftValue = indexOf(ALPHABET, Character.toLowerCase(input.nextLine().charAt(0)));
			
			String encryptedText = encrypt(text, shiftValue);
			System.out.println(encryptedText);
			System.out.println(decrypt(encryptedText, shiftValue));
		}
	}
	
	public static String encrypt(String text, int shift) {
		char[] textAsCharArray = text.toCharArray();
		char[] shiftedAlphabet = shiftAlphabet(ALPHABET, shift);
		
		for (int i = 0; i < textAsCharArray.length; i++) {
			if (contains(ALPHABET, Character.toLowerCase(textAsCharArray[i]))) {
				textAsCharArray[i] = shiftedAlphabet[indexOf(ALPHABET, Character.toLowerCase(textAsCharArray[i]))];
			}
		}
		return new String(textAsCharArray);
	}
	
	public static String decrypt(String text, int shift) {
		char[] textAsCharArray = text.toCharArray();
		char[] shiftedAlphabet = shiftAlphabet(ALPHABET, shift);
		
		for (int i = 0; i < textAsCharArray.length; i++) {
			if (contains(ALPHABET, Character.toLowerCase(textAsCharArray[i]))) {
				textAsCharArray[i] = ALPHABET[indexOf(shiftedAlphabet, Character.toLowerCase(textAsCharArray[i]))];
			}
		}
		return new String(textAsCharArray);
	}
	
	public static char[] shiftAlphabet(char[] alphabet, int amount) {
		alphabet = alphabet.clone();
		boolean end = false;
		do {
			if (amount<0) {
				amount += ALPHABET.length;
			} else if (amount>=ALPHABET.length) {
				amount -= ALPHABET.length;
			} else end = true;
		} while (!end);
		
		for (int i = 0; i < amount; i++) {
			char zs = alphabet[0];
			for (int j = 0; j < (alphabet.length-1); j++) {
				alphabet[j] = alphabet[j+1];
			}
			alphabet[alphabet.length-1] = zs;
		}
		return alphabet;
	}
	
	public static boolean contains(char[] array, char token) {
		for (int i = 0; i < array.length; i++) {
			if (array[i]==token) {
				return true;
			}
		}
		return false;
	}
	
	public static int indexOf(char[] array, char token) {
		for (int i = 0; i < array.length; i++) {
			if (array[i]==token) {
				return i;
			}
		}
		return -1;
	}
}

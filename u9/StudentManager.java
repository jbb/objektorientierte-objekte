import java.util.ArrayList;

public class StudentManager {
    static int partition(ArrayList<Student> a, int lo, int hi) {
        Student pivot = a.get(hi);
        int i = lo - 1;

        for(int j = lo; j < hi; j++) {
            if(a.get(j).compareTo(pivot) <= 0) {
                i++;
                Student t = a.get(j);
                a.set(j, a.get(i));
                a.set(i, t);
            }
        }
        i++;
        Student t = a.get(i);
        a.set(i, a.get(hi));
        a.set(hi, t);
        return i;
    }

    static void quicksortIntern(ArrayList<Student> a, int lo, int hi) {
        if (lo < hi) {
            int p = partition(a, lo, hi);
            quicksortIntern(a, lo, p - 1);
            quicksortIntern(a, p + 1, hi);
        }
    }

    static void quicksort(ArrayList<Student> a) {
        int lo = 0;
        int hi = a.size() - 1;

        quicksortIntern(a, lo, hi);
    }

    static ArrayList<Student> sortByEnrolmentNumber(java.util.ArrayList<Student> students) {
        quicksort(students);
        return students;
    }
}

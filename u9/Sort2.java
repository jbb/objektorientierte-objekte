public class Sort2 {
    static void bubblesort(int[] a) {
        int n = a.length;
        while(true) {
            boolean swapped = false;
            for (int i = 1; i < n; i++) {
                if (a[i - 1] > a[i]) {
                    int old = a[i - 1];
                    a[i - 1] = a[i];
                    a[i] = old;
                    swapped = true;
                }
            }

            if (!swapped) {
                break;
            }
        }
    }

    static int partition(int[] a, int lo, int hi) {
        int pivot = a[hi];
        int i = lo - 1;

        for (int j = lo; j < hi; j++) {
            if(a[j] <= pivot) {
                i++;
                int t = a[j];
                a[j] = a[i];
                a[i] = t;
            }
        }

        i++;
        int t = a[i];
        a[i] = a[hi];
        a[hi] = t;
        return i;
    }

    static void quicksortIntern(int[] a, int lo, int hi) {
        if (lo < hi) {
            int p = partition(a, lo, hi);
            quicksortIntern(a, lo, p - 1);
            quicksortIntern(a, p + 1, hi);
        }
    }

    static void quicksort(int[] a) {
        int lo = 0;
        int hi = a.length - 1;

        quicksortIntern(a, lo, hi);
    }
}

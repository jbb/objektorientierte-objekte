import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Test {
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		while(true) {
			System.out.println("Wähle ein Testprogramm:");
			System.out.println("[1]: StudentTest");
			System.out.println("[2]: SortTest");
			System.out.println("[3]: Performancevergleich der Algorithmen");
			System.out.println("[4]: Test if BubbleSort and QuickSort give the same Result");
			System.out.println("[5]: Quicksort mit unterschiedlicher vorsortierung");
			System.out.print("Eingabe: ");
			
			switch (input.nextInt()) {
			case 1:
				studentTest();
				break;
			case 2:
				sortTest();
				break;
			case 3:
				systemDemo();
				break;
			case 4:
				testIfBubbleSortAndQuickSortGiveSameResult();
				break;
			case 5:
				testQuickSort();
				break;
			default:
				break;
			}
			System.out.println("--------------------------------------------------------");
		}
	}
	
	public static void studentTest() {
        int[] arr = {5,4,3,2,1};
	    Sort.quicksort(arr);
        System.out.println(Arrays.toString(arr));

        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(2));
        students.add(new Student(1));
        students.add(new Student(3));
        
        StudentManager.sortByEnrolmentNumber(students);

        assert students.get(0).getEnrolmentNum() == 1;
        assert students.get(1).getEnrolmentNum() == 2;
        assert students.get(2).getEnrolmentNum() == 3;
    }
	
	public static void sortTest() {
		Scanner input = new Scanner(System.in);
		System.out.println("-----------------------------------");
		System.out.print("Wie groß soll der Array sein: ");
		int[] a = new int[input.nextInt()];
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for (int i = 0; i < a.length; i++) {
			a[i] = random.nextInt(Integer.MAX_VALUE);
		}
		System.out.println("Welcher Sortieralgorithmus soll verwendet werden: ");
		System.out.println("[1] Bubblesort");
		System.out.println("[2] Quicksort");
		System.out.println("[3] Bubblesort und Quicksort");
		System.out.print("Eingabe: ");
		long startTime, endTime;
		switch (input.nextInt()) {
		case 1:
			startTime = System.currentTimeMillis();
			Sort.bubblesort(a);
			endTime = System.currentTimeMillis();
			System.out.println("Zeit in ms: " + (endTime-startTime));
			break;
		case 2:
			startTime = System.currentTimeMillis();
			Sort.quicksort(a);
			endTime = System.currentTimeMillis();
			System.out.println("Zeit in ms: " + (endTime-startTime));
			break;
		case 3:
			int[] b = new int[a.length];
			for (int i = 0; i < a.length; i++) {
				b[i] = a[i];
			}
			System.out.println("Bubblesort:");
			startTime = System.currentTimeMillis();
			Sort.bubblesort(a);
			endTime = System.currentTimeMillis();
			System.out.println("Zeit in ms: " + (endTime-startTime));
			
			System.out.println("Quicksort:");
			startTime = System.currentTimeMillis();
			Sort.quicksort(b);
			endTime = System.currentTimeMillis();
			System.out.println("Zeit in ms: " + (endTime-startTime));
			break;
		default:
			break;
		}
	}
	
	public static int[] randomArray() {
        int[] randomElements = new int[100_000];
        ThreadLocalRandom r = ThreadLocalRandom.current();

        for (int i = 0; i < randomElements.length; i++) {
            randomElements[i] = r.nextInt();
        }

        return randomElements;
    }

    public static Student[] randomStudiArray() {
        Student[] randomElements = new Student[100_00];
        ThreadLocalRandom r = ThreadLocalRandom.current();

        for (int i = 0; i < randomElements.length; i++) {
            randomElements[i] = new Student(r.nextInt());
        }

        return randomElements;
    }

    public static void systemDemo() {
        long startTime;

        int[] randomElements = randomArray();
        
        startTime = System.currentTimeMillis();
        Sort.bubblesort(randomElements);
        System.out.printf("bubblesort took %d milliseconds\n", System.currentTimeMillis() - startTime);

        randomElements = randomArray();
        
        startTime = System.currentTimeMillis();
        Sort.quicksort(randomElements);
        System.out.printf("quicksort took %d milliseconds\n", System.currentTimeMillis() - startTime);

        Student[] randomStudis = randomStudiArray();
        
        startTime = System.currentTimeMillis();
        StudentManager.sortByEnrolmentNumber(new ArrayList<Student>(Arrays.asList(randomStudis)));
        System.out.printf("student sorting took %d milliseconds\n", System.currentTimeMillis() - startTime);
    }
    
    public static void testIfBubbleSortAndQuickSortGiveSameResult() {
		int[] a = new int[100000];
		int[] b = new int[100000];
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for (int i = 0; i < a.length; i++) {
			int zs = random.nextInt(Integer.MAX_VALUE);
			a[i] = zs;
			b[i] = zs;
		}
		
		Sort.bubblesort(a);
		Sort.quicksort(b);
		
		boolean ergebnis = true;
		
		for (int i = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				ergebnis = false;
			}
		}
		System.out.println(ergebnis);
	}
    
    public static void testQuickSort() {
		long startTime, endTime;
		Scanner input = new Scanner(System.in);
		System.out.print("Wie groß soll der Array sein: ");
		int[] a = new int[input.nextInt()];
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for (int i = 0; i < a.length; i++) {
			a[i] = random.nextInt(Integer.MAX_VALUE);
		}
		
		System.out.print("Zufällig: ");
		startTime = System.currentTimeMillis();
		Sort.quicksort(a);
		endTime = System.currentTimeMillis();
		System.out.println("Zeit in ms: " + (endTime-startTime));
		
		int[] b = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			b[i] = a[i];
		}
		
		System.out.print("sortiert aufsteigend: ");
		startTime = System.currentTimeMillis();
		Sort.quicksort(b);
		endTime = System.currentTimeMillis();
		System.out.println("Zeit in ms: " + (endTime-startTime));
		
		int[] c = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			c[i] = a[a.length-i-1];
		}
		
		System.out.print("sortiert absteigend: ");
		startTime = System.currentTimeMillis();
		Sort.quicksort(c);
		endTime = System.currentTimeMillis();
		System.out.println("Zeit in ms: " + (endTime-startTime));
	}
}

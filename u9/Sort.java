public class Sort {
	public static void bubblesort(int[] a) {
		boolean swapped = false;
		do {
			swapped = false;
			for (int i = 0; i < a.length-1; i++) {
				if (a[i]>a[i+1]) {
					int zs = a[i];
					a[i] = a[i+1];
					a[i+1] = zs;
					swapped = true;
				}
			}
		} while (swapped);
	}
	
	public static void quicksort(int[] a) {
		quicksort(a, 0, a.length-1);
	}
	
	public static void quicksort(int[] a, int low, int high) {
		if (low<high) {
			int m = partition(a, low, high);
			quicksort(a, low, m-1);
			quicksort(a, m+1, high);
		}
	}
	
	public static int partition(int[] a, int low, int high) {
		int pivot = a[low];
		int i = low;
		for (int j = low + 1; j < high + 1; j++) {
			if (a[j] < pivot) {
				i++;
				int zs = a[i];
				a[i] = a[j];
				a[j] = zs;
			}
		}
		int zs = a[i];
		a[i] = a[low];
		a[low] = zs;
		return i;
	}
}

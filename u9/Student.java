public class Student implements Comparable<Student> {
    int enrolmentNum;

    public Student(int num) {
        enrolmentNum = num;
    }

    public int getEnrolmentNum() {
        return enrolmentNum;
    }

    @Override
    public int compareTo(Student o) {
        if (enrolmentNum > o.enrolmentNum) {
            return 1;
        } else if (enrolmentNum == o.enrolmentNum) {
            return 0;
        } else {
            return -1;
        }
    }
}

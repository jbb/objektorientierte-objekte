# 9. Aufgabenblatt
## Aufgabe 1: Sortieren mit BubbleSort und QuickSort
a)+c) 
```
public class Sort {
	public static void bubblesort(int[] a) {
		boolean swapped = false;
		do {
			swapped = false;
			for (int i = 0; i < a.length-1; i++) {
				if (a[i]>a[i+1]) {
					int zs = a[i];
					a[i] = a[i+1];
					a[i+1] = zs;
					swapped = true;
				}
			}
		} while (swapped);
	}
	
	public static void quicksort(int[] a) {
		quicksort(a, 0, a.length-1);
	}
	
	public static void quicksort(int[] a, int low, int high) {
		if (low<high) {
			int m = partition(a, low, high);
			quicksort(a, low, m-1);
			quicksort(a, m+1, high);
		}
	}
	
	public static int partition(int[] a, int low, int high) {
		int pivot = a[low];
		int i = low;
		for (int j = low + 1; j < high + 1; j++) {
			if (a[j] < pivot) {
				i++;
				int zs = a[i];
				a[i] = a[j];
				a[j] = zs;
			}
		}
		int zs = a[i];
		a[i] = a[low];
		a[low] = zs;
		return i;
	}
}
```

b\) 14480ms

d\) 15ms

## Aufgabe 2: Sortieren mit Comparable
a\)
```
public class Student implements Comparable<Student> {
    int enrolmentNum;

    public Student(int num) {
        enrolmentNum = num;
    }

    public int getEnrolmentNum() {
        return enrolmentNum;
    }

    @Override
    public int compareTo(Student o) {
        if (enrolmentNum > o.enrolmentNum) {
            return 1;
        } else if (enrolmentNum == o.enrolmentNum) {
            return 0;
        } else {
            return -1;
        }
    }
}
```

b\)
```
import java.util.ArrayList;

public class StudentManager {
    static int partition(ArrayList<Student> a, int lo, int hi) {
        Student pivot = a.get(hi);
        int i = lo - 1;

        for(int j = lo; j < hi; j++) {
            if(a.get(j).compareTo(pivot) <= 0) {
                i++;
                Student t = a.get(j);
                a.set(j, a.get(i));
                a.set(i, t);
            }
        }
        i++;
        Student t = a.get(i);
        a.set(i, a.get(hi));
        a.set(hi, t);
        return i;
    }

    static void quicksortIntern(ArrayList<Student> a, int lo, int hi) {
        if (lo < hi) {
            int p = partition(a, lo, hi);
            quicksortIntern(a, lo, p - 1);
            quicksortIntern(a, p + 1, hi);
        }
    }

    static void quicksort(ArrayList<Student> a) {
        int lo = 0;
        int hi = a.size() - 1;

        quicksortIntern(a, lo, hi);
    }

    static ArrayList<Student> sortByEnrolmentNumber(java.util.ArrayList<Student> students) {
        quicksort(students);
        return students;
    }
}
```

c\) 12ms

## Aufgabe 3: Laufzeitmessungen
|Quicksort	|sortiert aufsteigend	|zufällig|sortiert absteigend|
|---		|---	|---	|---	|
|1.000		|1ms	|1ms	|2ms	|
|10.000		|19ms	|3ms	|51ms	|
|100.000	|1523ms	|15ms	|5177ms	|

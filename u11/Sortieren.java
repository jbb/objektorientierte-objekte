import java.util.concurrent.ThreadLocalRandom;

public class Sortieren {
	public static void main(String[] args) {
		int max = 10_000_000;
		int[] array = new int[1_000_000];
		fillIntArrayWithRandomNumbers(array, max);
		
		long startTime = System.currentTimeMillis();
		countingSort(array, max);
		long endTime = System.currentTimeMillis();
		System.out.println("Zeit in ms: " + (endTime-startTime));
		
		//for (int i = 0; i < array.length; i++) {
		//	System.out.println(array[i]);
		//}
	}
	
	public static void fillIntArrayWithRandomNumbers(int[] array, int max) {
		ThreadLocalRandom random = ThreadLocalRandom.current();
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(max);
		}
	}
	
	public static void countingSort(int[] array, int max) {
		int[] anzahl = new int[max];
		for (int i = 0; i < array.length; i++) {
			anzahl[array[i]]++;
		}
		
		for (int i = 1; i < anzahl.length; i++) {
			anzahl[i] += anzahl[i-1];
		}
		
		int[] originalAnzahl = new int[max];
		for (int i = 0; i < anzahl.length; i++) {
			originalAnzahl[i] = anzahl[i];
		}
		
		for (int i = 0; i < array.length; i++) {
			boolean swapped = false;
			do {
				swapped = false;
				if (i > originalAnzahl[array[i]] || i < anzahl[array[i]]) {
					anzahl[array[i]]--;
					int zs = array[anzahl[array[i]]];
					array[anzahl[array[i]]] = array[i];
					array[i] = zs;
					swapped = true;
				}
			} while (swapped);
		}
	}
}

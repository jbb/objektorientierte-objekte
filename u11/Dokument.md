# 11. Aufgabenblatt
## Aufgabe 1: Sortieren
a)
```Java
public static void countingSort(int[] array, int max) {
	int[] anzahl = new int[max];
	for (int i = 0; i < array.length; i++) {
		anzahl[array[i]]++;
	}
	
	for (int i = 1; i < anzahl.length; i++) {
		anzahl[i] += anzahl[i-1];
	}
	
	int[] originalAnzahl = new int[max];
	for (int i = 0; i < anzahl.length; i++) {
		originalAnzahl[i] = anzahl[i];
	}
	
	for (int i = 0; i < array.length; i++) {
		boolean swapped = false;
		do {
			swapped = false;
			if (i > originalAnzahl[array[i]] || i < anzahl[array[i]]) {
				anzahl[array[i]]--;
				int zs = array[anzahl[array[i]]];
				array[anzahl[array[i]]] = array[i];
				array[i] = zs;
				swapped = true;
			}
		} while (swapped);
	}
}
```

b)
- Best-Case: O(n+k)
- Worst-Case: O(n+k)

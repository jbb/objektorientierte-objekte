#!/usr/bin/env python3

from typing import List, Tuple


class Stack:
    __elements: List[float]

    def __init__(self):
        self.__elements = []

    def empty(self) -> bool:
        return self.__elements == []

    def push(self, value: float) -> None:
        self.__elements.append(value)

    def pop(self) -> float:
        return self.__elements.pop()

    def peek(self):
        return self.__elements[-1]


class Queue:
    __elements: List[float]
    __begin: int
    __end: int

    def __init__(self):
        self.__elements = 1024 * [None]
        self.__begin = 0
        self.__end = -1

    def push(self, value: float):
        # If the new element would exceed the size of the list,
        # insert it at the beginning
        if self.__end > 1022:
            self.__end = -1

        self.__end += 1
        self.__elements[self.__end] = value

    def pop(self):
        val = self.__elements[self.__begin]
        if self.__begin > 1022:
            self.__begin = -1

        self.__begin += 1
        return val

    def peek(self) -> float:
        return self.__elements[self.__begin]

    def empty(self) -> bool:
        return self.__begin == (self.__end + 1)


class Rechteck:
    __mid: Tuple[int, int]
    __edge_length: Tuple[int, int]

    def __init__(self, mid: Tuple[int, int], edge_lengths: Tuple[int, int]):
        self.__mid = mid
        self.__edge_length = edge_lengths

    def area(self) -> int:
        (w, h) = self.__edge_length
        return w * h


class Quader(Rechteck):
    __height: int

    def __init__(self, mid: Tuple[int, int], edge_lengths: Tuple[int, int], height: int):
        super().__init__(mid, edge_lengths)
        self.__height = height

    def volume(self) -> int:
        return super().area() * self.__height


if __name__ == "__main__":
    # Stack
    stack = Stack()
    assert stack.empty()

    stack.push(4.0)
    assert stack.peek() == 4.0

    stack.push(644.3)
    assert stack.peek() == 644.3

    assert stack.pop() == 644.3
    assert stack.peek() == 4.0

    stack.pop()
    assert stack.empty()

    # Queue
    queue = Queue()
    assert queue.empty()

    queue.push(4.1)
    assert queue.peek() == 4.1

    queue.push(5.2)
    assert queue.pop() == 4.1
    assert queue.pop() == 5.2
    assert queue.empty()

    for i in range(0, 2000):
        queue.push(i)
        assert queue.pop() == i

    # Rechteck
    rectangle = Rechteck((0, 0), (4, 6))
    assert rectangle.area() == 24

    quader = Quader((0, 0), (4, 6), 5)
    assert quader.volume() == 120

Name, Geburtsdatum und Wohnort muss die Uni über Professor*innen und Studierende wissen. Sie beziehen sich generell auf Personen, und sind deswegen als Attribute der Klasse Person umgesetzt.
Der Name wird in einem String gespeichert, da eine Aufteilung in Vor- und Nachnamen mehr Probleme schafft als sie lösen würde. Sie würde die eingabe verkomplizieren und nicht jede/r hat genau einen Vornamen und einen Nachnamen.

Studierende haben zusätzlich eine Matrikelnummer, ihr Semesterticket verlängert (oder auch nicht) und Module für jedes Semester. Das Semester wird in einem Integer gespeichert, genauso wie die ID der gewählten Module. Beide befinden sich dann in einem dictionary.

Die Professor*innen haben im Gegensatz zu den Studierenden publikationen, die wieder über eine integer ID zugeordnet werden, und gehören einem Fachbereich an.

import java.util.ArrayList;

public class Professor_in extends Person {
	public enum Fachbereich {
		MathematikInformatik,
		Biologie,
		Physik,
		Chemie,
		Politikwissenschaft
	}
	
	public Professor_in(String name, String geburtsDatum, String wohnort, Fachbereich fachbereich) {
		super(name, geburtsDatum, wohnort);

		this.fachbereich = fachbereich;
		this.publikationen = new ArrayList<Integer>();
	}
	
	ArrayList<Integer> publikationen;
	Fachbereich fachbereich;
}

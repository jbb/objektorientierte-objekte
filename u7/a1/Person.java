public class Person {
	public Person(String name, String geburtsDatum, String wohnort) {
		this.name = name;
		this.geburtsDatum = geburtsDatum;
		this.wohnort = wohnort;
	}
	
	public String name;
	public String geburtsDatum;
	public String wohnort;
}

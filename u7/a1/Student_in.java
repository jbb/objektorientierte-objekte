import java.util.HashMap;

public class Student_in extends Person{
	public Student_in(String name, String geburtsDatum, String wohnort, int matrikelnummer, boolean semTicketVerlängert, HashMap<Integer, Integer> module) {
		super(name, geburtsDatum, wohnort);

		this.matrikelnummer = matrikelnummer;
		this.semesterTicketVerlängert = semTicketVerlängert;
		this.module = module;
	}
	
	int matrikelnummer;
	boolean semesterTicketVerlängert;
	HashMap<Integer, Integer> module;
}

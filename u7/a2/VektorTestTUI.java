import java.util.Scanner;

public class VektorTestTUI {
	static Scanner input = new Scanner(System.in);
	static int einrueckung = 0;
	enum Dimension {keine, Vektor2D, Vektor3D}
	static Dimension aktuellesProgramm = Dimension.keine;
	
	public static void main(String[] args) {
		println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		do {
			println("██╗    ██╗██╗██╗     ██╗     ██╗  ██╗ ██████╗ ███╗   ███╗███╗   ███╗███████╗███╗   ██╗");
			println("██║    ██║██║██║     ██║     ██║ ██╔╝██╔═══██╗████╗ ████║████╗ ████║██╔════╝████╗  ██║");
			println("██║ █╗ ██║██║██║     ██║     █████╔╝ ██║   ██║██╔████╔██║██╔████╔██║█████╗  ██╔██╗ ██║");
			println("██║███╗██║██║██║     ██║     ██╔═██╗ ██║   ██║██║╚██╔╝██║██║╚██╔╝██║██╔══╝  ██║╚██╗██║");
			println("╚███╔███╔╝██║███████╗███████╗██║  ██╗╚██████╔╝██║ ╚═╝ ██║██║ ╚═╝ ██║███████╗██║ ╚████║");
			println(" ╚══╝╚══╝ ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝");
			println("Wähle die Dimension:");
			println("[1] Vektor2D");
			println("[2] Vektor3D");
			println("[9] Programm beenden");
			print("Eingabe: ");
			switch (input.nextInt()) {
			case 1:
				aktuellesProgramm = Dimension.Vektor2D;
				try {
					vektor();
				} catch (Exception e) {
					// TODO: handle exception
				}
				aktuellesProgramm = Dimension.keine;
				break;
			case 2:
				aktuellesProgramm = Dimension.Vektor3D;
				try {
					vektor();
				} catch (Exception e) {
					// TODO: handle exception
				}
				aktuellesProgramm = Dimension.keine;
				break;
			case 9:
				println("ENDE");
				System.exit(1);
				break;
			default:
				println("Zahl nicht bekannt.\n\n\n\n");
				break;
			}
		} while (true);
	}
	
	public static void vektor() throws UnknownDimensionException {
		boolean end = false;
		Vektor2D vec2D = new Vektor2D();
		Vektor3D vec3D = new Vektor3D();
		println("\n\n\n");
		
		switch (aktuellesProgramm) {
		case Vektor2D:
			println("██╗   ██╗███████╗██╗  ██╗████████╗ ██████╗ ██████╗ ██████╗ ██████╗ ");
			println("██║   ██║██╔════╝██║ ██╔╝╚══██╔══╝██╔═══██╗██╔══██╗╚════██╗██╔══██╗");
			println("██║   ██║█████╗  █████╔╝    ██║   ██║   ██║██████╔╝ █████╔╝██║  ██║");
			println("╚██╗ ██╔╝██╔══╝  ██╔═██╗    ██║   ██║   ██║██╔══██╗██╔═══╝ ██║  ██║");
			println(" ╚████╔╝ ███████╗██║  ██╗   ██║   ╚██████╔╝██║  ██║███████╗██████╔╝");
			println("  ╚═══╝  ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═════╝ ");
			println("-------------------------------------------------------------------");
			break;
		case Vektor3D:
			println("██╗   ██╗███████╗██╗  ██╗████████╗ ██████╗ ██████╗ ██████╗ ██████╗ ");
			println("██║   ██║██╔════╝██║ ██╔╝╚══██╔══╝██╔═══██╗██╔══██╗╚════██╗██╔══██╗");
			println("██║   ██║█████╗  █████╔╝    ██║   ██║   ██║██████╔╝ █████╔╝██║  ██║");
			println("╚██╗ ██╔╝██╔══╝  ██╔═██╗    ██║   ██║   ██║██╔══██╗ ╚═══██╗██║  ██║");
			println(" ╚████╔╝ ███████╗██║  ██╗   ██║   ╚██████╔╝██║  ██║██████╔╝██████╔╝");
			println("  ╚═══╝  ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═════╝ ");
			println("-------------------------------------------------------------------");
			break;
		default:
			throw new UnknownDimensionException();
		}
		
		println("Wähle eine Operation:");
		println("[1] isNullvektor");
		println("[2] add");
		println("[3] sub");
		println("[4] mult");
		println("[5] div");
		println("[6] normalize");
		println("[7] truncate");
		println("[9] Zurück zum Hauptmenü");
		do {
			switch (aktuellesProgramm) {
			case Vektor2D:
				println("Aktueller zwischenspeicher: " + vec2D.toString());
				break;
			case Vektor3D:
				println("Aktueller zwischenspeicher: " + vec3D.toString());
				break;
			default:
				throw new UnknownDimensionException();
			}
			print("Eingabe: ");
			switch (input.nextInt()) {
			case 1:
				switch (aktuellesProgramm) {
				case Vektor2D:
					if (vec2D.isNullvector()) {
						println("Aktueller Vektor ist ein Nullvektor!");
					} else println("Aktueller Vektor ist kein Nullvektor!");
					break;
				case Vektor3D:
					if (vec3D.isNullvector()) {
						println("Aktueller Vektor ist ein Nullvektor!");
					} else println("Aktueller Vektor ist kein Nullvektor!");
					break;
				default:
					throw new UnknownDimensionException();
				}
				break;
			case 2:
				println("Gib den zu addierenden Vektor ein:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D.add(inputVektor2D());
					break;
				case Vektor3D:
					vec3D.add(inputVektor3D());
					break;
				default:
					throw new UnknownDimensionException();
				}
				break;
			case 3:
				println("Gib den zu subtrahierenden Vektor ein:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D.sub(inputVektor2D());
					break;
				case Vektor3D:
					vec3D.sub(inputVektor3D());
					break;
				default:
					throw new UnknownDimensionException();
				}
				break;
			case 4:
				println("Gib die zu multiplizierende Kommazahl ein:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D.mult(inputFloat());
					break;
				case Vektor3D:
					vec3D.mult(inputFloat());
					break;
				default:
					throw new UnknownDimensionException();
				}
				break;
			case 5:
				println("Gib die zu dividierende Kommazahl ein:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D.div(inputFloat());
					break;
				case Vektor3D:
					vec3D.div(inputFloat());
					break;
				default:
					throw new UnknownDimensionException();
				}
				
				break;
			case 6:
				println("Normalisiere...");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D.normalize();
					break;
				case Vektor3D:
					vec3D.normalize();
					break;
				default:
					throw new UnknownDimensionException();
				}
				
				break;
			case 7:
				println("Gib einen Float an um die Zahl zu runden:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D.truncate(inputFloat());
					break;
				case Vektor3D:
					vec3D.truncate(inputFloat());
					break;
				default:
					throw new UnknownDimensionException();
				}
				break;
			case 9:
				end = true;
				break;
			default:
				println("Zahl nicht bekannt.");
				break;
			}
		} while (!end);
		println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
	
	public static float inputFloat() throws UnknownDimensionException {
		einrueckung++;
		
		boolean ende = false;
		Float f = 0.0f;
		Vektor2D vec2D = new Vektor2D();
		Vektor3D vec3D = new Vektor3D();
		do {
			String eingabe = input.next();
			switch (eingabe.toLowerCase()) {
			case "length":
				println("Gib den Vektor an dessen Länge gebraucht wird:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D = inputVektor2D();
					f = vec2D.length();
					break;
				case Vektor3D:
					vec3D = inputVektor3D();
					f = vec3D.length();
					break;
				default:
					throw new UnknownDimensionException();
				}
				ende = true;
				break;
			case "lengthsquare":
				println("Gib den Vektor an dessen Länge zum Quadrat gebraucht wird:");
				switch (aktuellesProgramm) {
				case Vektor2D:
					vec2D = inputVektor2D();
					f = vec2D.lengthSquare();
					break;
				case Vektor3D:
					vec3D = inputVektor3D();
					f = vec3D.lengthSquare();
					break;
				default:
					throw new UnknownDimensionException();
				}
				ende = true;
				break;
			default:
				eingabe = eingabe.replace(',', '.');
				try {
					f = Float.parseFloat(eingabe);
					ende = true;
				} catch (Exception e) {
					einrueckung--;
					print("Falsche Eingabe! Neuer Versuch: ");
					einrueckung++;
				}
				break;
			}
		} while (!ende);
		
		einrueckung--;
		return f;
	}
	
	public static Vektor2D inputVektor2D() throws UnknownDimensionException {
		//einrueckung++;
		print("Erste Zahl des Vektors: ");
		Float tmp1 = inputFloat();
		print("Zweite Zahl des Vektors: ");
		Float tmp2 = inputFloat();
		//einrueckung--;
		return new Vektor2D(tmp1, tmp2);
		
	}
	
	public static Vektor3D inputVektor3D() throws UnknownDimensionException {
		//einrueckung++;
		print("Erste Zahl des Vektors: ");
		Float tmp1 = inputFloat();
		print("Zweite Zahl des Vektors: ");
		Float tmp2 = inputFloat();
		print("Dritte Zahl des Vektors: ");
		Float tmp3 = inputFloat();
		//einrueckung--;
		return new Vektor3D(tmp1, tmp2, tmp3);
	}
	
	public static void print(String text) {
		for (int i = 0; i < einrueckung; i++) {
			System.out.print("|   ");
		}
		System.out.print(text);
	}
	
	public static void println(String text) {
		print(text + "\n");
	}
}

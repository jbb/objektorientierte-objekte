
public class Vektor2D {
	public float x, y;
	
	public Vektor2D() {
		this(0, 0);
	}
	
	public Vektor2D(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean isNullvector() {
		if (x == 0 && y == 0) {
			return true;
		} else return false;
	}
	
	public void add(Vektor2D vec) {
		x += vec.x;
		y += vec.y;
	}
	
	public void sub(Vektor2D vec) {
		x -= vec.x;
		y -= vec.y;
	}
	
	public void mult(float s) {
		x *= s;
		y *= s;
	}
	
	public boolean div(float s) {
		if (s != 0.0f) {
			x /= s;
			y /= s;
			return true;
		} else return false;
	}
	
	public float length() {
		return (float)Math.sqrt(x*x + y*y);
	}
	
	public float lengthSquare() {
		return (float)(x*x + y*y);
	}
	
	public boolean isEqual(Vektor2D vec) {
		if (x == vec.x && y == vec.y) {
			return true;
		} else return false;
	}
	
	public boolean isNotEqual(Vektor2D vec) {
		if (isEqual(vec)) {
			return false;
		} else return true;
	}
	
	public void normalize() {
		if (isNullvector()) {
			div(length() + 0.00001f);
		} else {
			div(length());
		}
	}
	
	public void truncate(float max) {
		//TODO
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}

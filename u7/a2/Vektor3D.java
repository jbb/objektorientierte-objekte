
public class Vektor3D {
	public float x, y, z;
	
	public Vektor3D() {
		this(0, 0, 0);
	}
	
	public Vektor3D(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public boolean isNullvector() {
		if (x == 0 && y == 0 && z == 0) {
			return true;
		} else return false;
	}
	
	public void add(Vektor3D vec) {
		x += vec.x;
		y += vec.y;
		z += vec.z;
	}
	
	public void sub(Vektor3D vec) {
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
	}
	
	public void mult(float s) {
		x *= s;
		y *= s;
		z *= s;
	}
	
	public boolean div(float s) {
		if (s != 0.0f) {
			x /= s;
			y /= s;
			z /= s;
			return true;
		} else return false;
	}
	
	public float length() {
		return (float)Math.sqrt(x*x + y*y + z*z);
	}
	
	public float lengthSquare() {
		return (float)(x*x + y*y + z*z);
	}
	
	public boolean isEqual(Vektor3D vec) {
		if (x == vec.x && y == vec.y && z == vec.z) {
			return true;
		} else return false;
	}
	
	public boolean isNotEqual(Vektor3D vec) {
		if (isEqual(vec)) {
			return false;
		} else return true;
	}
	
	public void normalize() {
		if (isNullvector()) {
			div(length() + 0.00001f);
		} else {
			div(length());
		}
	}
	
	public void truncate(float max) {
		//TODO
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + "," + z + ")";
	}
}

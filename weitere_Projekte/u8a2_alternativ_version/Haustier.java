package a2;

public interface Haustier {
	public String getName();
	public int getAlter();
	public String getBezeichnung();
	public String getTierstimme();
}
package a2;

public class Haustierhalter {
	private Haustier meinHaustier;
	public Haustierhalter() {
	
	}
	public void neuesHaustier(Haustier haustier) {
		meinHaustier = haustier;
	}
	public String getHaustierbezeichnung() {
		return meinHaustier.getBezeichnung();
	}
}
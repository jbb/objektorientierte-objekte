package a2;

/**
 * @author paul
 *
 */
public class HaustierHaushalt {
	public static void main(String[] args) {
		Haustierhalter heinz = new Haustierhalter();
		Hund rambo = new Hund("Rambo", 3);
		heinz.neuesHaustier(rambo);
		Katze bantam = new Katze("Bantam", 5);
		heinz.neuesHaustier(bantam);
		System.out.println("Haustier von Heinz: "+
			heinz.getHaustierbezeichnung());
		System.out.println("Alter von Rambo: "+
				rambo.getAlter());
		}
}

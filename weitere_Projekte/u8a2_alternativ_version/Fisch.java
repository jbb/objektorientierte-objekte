package a2;

public class Fisch implements Haustier {
	private String name;
	private int alter;
	
	public Fisch(String name, int alter) {
		this.name = name;
		this.alter = alter;
	}
	public String getName() {
		return name;
	}
	public int getAlter() {
		return alter;
	}
	public String getBezeichnung() {
		return "Fisch";
	}
	public String getTierstimme() {
		return "blub";
	}
}

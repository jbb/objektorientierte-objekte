package a2;

public class Frosch implements Haustier {
	private String name;
	private int alter;
	
	public Frosch(String name, int alter) {
		this.name = name;
		this.alter = alter;
	}
	public String getName() {
		return name;
	}
	public int getAlter() {
		return alter;
	}
	public String getBezeichnung() {
		return "Frosch";
	}
	public String getTierstimme() {
		return "quack";
	}
}

import java.util.Scanner;

public class VektorTestTUI {
	static Scanner input = new Scanner(System.in);
	static int einrueckung = 0;
	static int dimension = 0;
	
	public static void main(String[] args) {
		println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		do {
			println("██╗    ██╗██╗██╗     ██╗     ██╗  ██╗ ██████╗ ███╗   ███╗███╗   ███╗███████╗███╗   ██╗");
			println("██║    ██║██║██║     ██║     ██║ ██╔╝██╔═══██╗████╗ ████║████╗ ████║██╔════╝████╗  ██║");
			println("██║ █╗ ██║██║██║     ██║     █████╔╝ ██║   ██║██╔████╔██║██╔████╔██║█████╗  ██╔██╗ ██║");
			println("██║███╗██║██║██║     ██║     ██╔═██╗ ██║   ██║██║╚██╔╝██║██║╚██╔╝██║██╔══╝  ██║╚██╗██║");
			println("╚███╔███╔╝██║███████╗███████╗██║  ██╗╚██████╔╝██║ ╚═╝ ██║██║ ╚═╝ ██║███████╗██║ ╚████║");
			println(" ╚══╝╚══╝ ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝");
			println("Gib an wie viel Dimensionen die Vektoren haben sollen!");
			println("Gib 0 ein um das Programm beenden.");
			print("Eingabe: ");
			int eingabe = input.nextInt();
			if (eingabe == 0) {
				System.out.println("ENDE");
				System.exit(1);
			} else if (eingabe > 0) {
				dimension = eingabe;
				vektor();
				dimension = 0;
			} else {
				System.out.println("Falsche Eingabe!");
			}
		} while (true);
	}
	
	public static void vektor() {
		boolean end = false;
		Vektor vec = new Vektor(dimension);
		println("\n\n\n");
		
		println("██╗   ██╗███████╗██╗  ██╗████████╗ ██████╗ ██████╗     ██╗   ██╗███╗   ██╗██╗     ██╗███╗   ███╗██╗████████╗███████╗██████╗ ");
		println("██║   ██║██╔════╝██║ ██╔╝╚══██╔══╝██╔═══██╗██╔══██╗    ██║   ██║████╗  ██║██║     ██║████╗ ████║██║╚══██╔══╝██╔════╝██╔══██╗");
		println("██║   ██║█████╗  █████╔╝    ██║   ██║   ██║██████╔╝    ██║   ██║██╔██╗ ██║██║     ██║██╔████╔██║██║   ██║   █████╗  ██║  ██║");
		println("╚██╗ ██╔╝██╔══╝  ██╔═██╗    ██║   ██║   ██║██╔══██╗    ██║   ██║██║╚██╗██║██║     ██║██║╚██╔╝██║██║   ██║   ██╔══╝  ██║  ██║");
		println(" ╚████╔╝ ███████╗██║  ██╗   ██║   ╚██████╔╝██║  ██║    ╚██████╔╝██║ ╚████║███████╗██║██║ ╚═╝ ██║██║   ██║   ███████╗██████╔╝");
		println("  ╚═══╝  ╚══════╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝     ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚═╝╚═╝     ╚═╝╚═╝   ╚═╝   ╚══════╝╚═════╝ ");
		println("----------------------------------------------------------------------------------------------------------------------------");
		
		println("Wähle eine Operation:");
		println("[1] isNullvektor");
		println("[2] add");
		println("[3] sub");
		println("[4] mult");
		println("[5] div");
		println("[6] normalize");
		println("[7] truncate");
		println("[9] Zurück zum Hauptmenü");
		
		do {
			println("Aktueller zwischenspeicher: " + vec.toString());
			print("Eingabe: ");
			switch (input.nextInt()) {
			case 1:
				if (vec.isNullvector()) {
					println("Aktueller Vektor ist ein Nullvektor!");
				} else println("Aktueller Vektor ist kein Nullvektor!");
				break;
			case 2:
				println("Gib den zu addierenden Vektor ein:");
				try {
					vec.add(inputVektor());
				} catch (DifferentDimensionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 3:
				println("Gib den zu subtrahierenden Vektor ein:");
				try {
					vec.sub(inputVektor());
				} catch (DifferentDimensionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 4:
				println("Gib die zu multiplizierende Kommazahl ein:");
				vec.mult(inputFloat());
				break;
			case 5:
				println("Gib die zu dividierende Kommazahl ein:");
				vec.div(inputFloat());
				break;
			case 6:
				println("Normalisiere...");
				vec.normalize();
				break;
			case 7:
				println("Gib einen Float an um die Zahl zu runden:");
				vec.truncate(inputFloat());
				break;
			case 9:
				end = true;
				break;
			default:
				println("Zahl nicht bekannt.");
				break;
			}
		} while (!end);
		println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
	
	public static float inputFloat(){
		einrueckung++;
		
		boolean ende = false;
		Float f = 0.0f;
		Vektor vec = new Vektor(dimension);
		do {
			String eingabe = input.next();
			switch (eingabe.toLowerCase()) {
			case "length":
				println("Gib den Vektor an dessen Länge gebraucht wird:");
				vec = inputVektor();
				f = vec.length();
				ende = true;
				break;
			case "lengthsquare":
				println("Gib den Vektor an dessen Länge zum Quadrat gebraucht wird:");
				vec = inputVektor();
				f = vec.lengthSquare();
				ende = true;
				break;
			default:
				eingabe = eingabe.replace(',', '.');
				try {
					f = Float.parseFloat(eingabe);
					ende = true;
				} catch (Exception e) {
					einrueckung--;
					print("Falsche Eingabe! Neuer Versuch: ");
					einrueckung++;
				}
				break;
			}
		} while (!ende);
		
		einrueckung--;
		return f;
	}
	
	public static Vektor inputVektor() {
		//einrueckung++;
		Vektor returnValue = new Vektor(dimension);
		for (int i = 0; i < dimension; i++) {
			print((i+1) + ". Zahl des Vektors: ");
			returnValue.werte[i] = inputFloat();
		}
		//einrueckung--;
		return returnValue;
	}
	
	public static void print(String text) {
		for (int i = 0; i < einrueckung; i++) {
			System.out.print("|   ");
		}
		System.out.print(text);
	}
	
	public static void println(String text) {
		print(text + "\n");
	}
}

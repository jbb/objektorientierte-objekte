
public class Vektor {
	float[] werte;
	
	public Vektor(int dimension) {
		werte = new float[dimension];
		for (int i = 0; i < dimension; i++) {
			werte[i] = 0;
		}
	}
	
	public Vektor(float[] werte) {
		this.werte = new float[werte.length];
		this.werte = werte;
	}
	
	public boolean isNullvector() {
		boolean returnValue = true;
		for (int i = 0; i < werte.length; i++) {
			if (werte[i] != 0) {
				returnValue = false;
			}
		}
		return returnValue;
	}
	
	public void add(Vektor vec) throws DifferentDimensionException {
		if (werte.length == vec.werte.length) {
			for (int i = 0; i < werte.length; i++) {
				werte[i] += vec.werte[i];
			}
		} else {
			throw new DifferentDimensionException();
		}
	}
	
	public void sub(Vektor vec) throws DifferentDimensionException  {
		if (werte.length == vec.werte.length) {
			for (int i = 0; i < werte.length; i++) {
				werte[i] -= vec.werte[i];
			}
		} else {
			throw new DifferentDimensionException();
		}
	}
	
	public void mult(float s) {
		for (int i = 0; i < werte.length; i++) {
			werte[i] *= s;
		}
	}
	
	public boolean div(float s) {
		if (s != 0.0f) {
			for (int i = 0; i < werte.length; i++) {
				werte[i] /= s;
			}
			return true;
		} else return false;
	}
	
	public float length() {
		return (float)Math.sqrt(lengthSquare());
	}
	
	public float lengthSquare() {
		float wert = 0;
		for (int i = 0; i < werte.length; i++) {
			wert += i*i;
		}
		return wert;
	}
	
	public boolean isEqual(Vektor vec) {
		boolean returnValue = true;
		if (werte.length == vec.werte.length) {
			for (int i = 0; i < werte.length; i++) {
				if (werte[i] != vec.werte[i]) {
					returnValue = false;
				}
			}
		} else {
			returnValue = false;
		}
		return returnValue;
	}
	
	public boolean isNotEqual(Vektor vec) {
		return !isEqual(vec);
	}
	
	public void normalize() {
		if (isNullvector()) {
			div(length() + 0.00001f);
		} else {
			div(length());
		}
	}
	
	public void truncate(float max) {
		for (int i = 0; i < werte.length; i++) {
			while (length() >= max) {
				div(1.00001f);
			}
		}
		
	}
	
	@Override
	public String toString() {
		String returnValue = "(";
		for (int i = 0; i < werte.length-1; i++) {
			returnValue += werte[i] + ",";
		}
		returnValue += werte[werte.length-1] + ")";
		return returnValue;
	}
}

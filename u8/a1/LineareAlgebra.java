
public class LineareAlgebra {
	private LineareAlgebra() {
	}
	
	public static Vektor2D add(Vektor2D vec1, Vektor2D vec2) {
		return new Vektor2D(vec1.x+vec2.x, vec1.y+vec2.y);
	}
	
	public static Vektor3D add(Vektor3D vec1, Vektor3D vec2) {
		return new Vektor3D(vec1.x+vec2.x, vec1.y+vec2.y, vec1.z+vec2.z);
	}
	
	public static Vektor2D sub(Vektor2D vec1, Vektor2D vec2) {
		return new Vektor2D(vec1.x-vec2.x, vec1.y-vec2.y);
	}
	
	public static Vektor3D sub(Vektor3D vec1, Vektor3D vec2) {
		return new Vektor3D(vec1.x-vec2.x, vec1.y-vec2.y, vec1.z-vec2.z);
	}
	
	public static Vektor2D mult(Vektor2D vec, Float f) {
		return new Vektor2D(vec.x*f, vec.y*f);
	}
	
	public static Vektor3D mult(Vektor3D vec, Float f) {
		return new Vektor3D(vec.x*f, vec.y*f, vec.z*f);
	}
	
	public static Vektor2D div(Vektor2D vec, Float f) throws DivByZeroException {
		if (f != 0) {
			return new Vektor2D(vec.x/f, vec.y/f);
		} else throw new DivByZeroException();
	}
	
	public static Vektor3D div(Vektor3D vec, Float f) throws DivByZeroException {
		if (f != 0) {
			return new Vektor3D(vec.x/f, vec.y/f, vec.z/f);
		} else throw new DivByZeroException();
	}
	
	public static double length(Vektor2D vec) {
		return Math.sqrt(vec.x*vec.x + vec.y*vec.y);
	}
	
	public static double length(Vektor3D vec) {
		return Math.sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
	}
	
	public static double euklDistanz(Vektor2D vec1, Vektor2D vec2) {
		return length(sub(vec2, vec1));
	}
	
	public static double euklDistanz(Vektor3D vec1, Vektor3D vec2) {
		return length(sub(vec2, vec1));
	}
	
	public static Vektor2D normalize(Vektor2D vec) {
		try {
			if (vec.isNullvector()) {
				return div(vec, (float)length(vec) + 0.00001f);
			} else {
				return div(vec, (float)length(vec));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Vektor2D();
		}
	}
	
	public static Vektor3D normalize(Vektor3D vec) {
		try {
			if (vec.isNullvector()) {
				return div(vec, (float)length(vec) + 0.00001f);
			} else {
				return div(vec, (float)length(vec));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Vektor3D();
		}
	}
	
	public static double dotProduct(Vektor2D vec1, Vektor2D vec2) {
		return vec1.x*vec2.x + vec1.y*vec2.y;
	}
	
	public static double dotProduct(Vektor3D vec1, Vektor3D vec2) {
		return vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
	}
	
	public static double kosinusFormel(Vektor2D vec1, Vektor2D vec2) {
		return dotProduct(vec1, vec2) / (length(vec1)*length(vec2));
	}
	
	public static double kosinusFormel(Vektor3D vec1, Vektor3D vec2) {
		return dotProduct(vec1, vec2) / (length(vec1)*length(vec2));
	}
	
	public static Vektor2D truncate(Vektor2D vec, double max) {
		return new Vektor2D(
				(float)Math.min(vec.x, max),
				(float)Math.min(vec.y, max));
	}
	
	public static Vektor3D truncate(Vektor3D vec, double max) {
		return new Vektor3D(
				(float)Math.min(vec.x, max),
				(float)Math.min(vec.y, max),
				(float)Math.min(vec.z, max));
	}

	public static void main(String[] args) {
		Vektor2D res = LineareAlgebra.sub(new Vektor2D(4.0f, 2.0f), new Vektor2D(2.0f, 1.0f));
		System.out.printf("%f %f\n", res.x, res.y);
		assert res.x == 2.0f && res.y == 1.0f;

		// Probleme während der Entwicklung:
		//  - Vektor direkt mit Vektor verglichen,
		//    returnt immer false wenn es nicht die selbe instanz ist

		Vektor2D res1 = LineareAlgebra.truncate(new Vektor2D(4.0f, 4.0f),  3.0);
		System.out.printf("%f %f\n", res1.x, res1.y);
		assert res1.x == 3.0f && res1.y == 3.0f;

		// Probleme während der Entwicklung:
		//  - min und max verwechselt (gefuden du Test)
		//  - Falscher Variablenname verwendet, res statt res1
	}
}

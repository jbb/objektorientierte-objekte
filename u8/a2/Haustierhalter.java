import java.util.ArrayList;

public class Haustierhalter {
	private ArrayList<Haustier> meineHaustiere;

	public Haustierhalter() {
		this.meineHaustiere = new ArrayList<Haustier>();
	}
	
	public void neuesHaustier(Haustier haustier) {
		meineHaustiere.add(haustier);
	}
	
	public void removeHaustier(int index) throws IndexOutOfBoundsException {
		meineHaustiere.remove(index);
	}
	
	public String getHaustierbezeichnungen() {
		String returnValue = "";
		for (int i = 0; i < meineHaustiere.size()-1; i++) {
			returnValue += meineHaustiere.get(i).getBezeichnung() + ", ";
		}
		returnValue += meineHaustiere.get(meineHaustiere.size()-1).getBezeichnung();
		return returnValue;
	}
}

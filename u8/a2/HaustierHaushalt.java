
public class HaustierHaushalt {
	public static void main(String[] args) {
		Haustierhalter heinz = new Haustierhalter();
		Hund rambo = new Hund("Rambo", 3);
		heinz.neuesHaustier(rambo);
		
		Papagei polly = new Papagei("Polly", 3);
		heinz.neuesHaustier(polly);
		System.out.println(polly.getName() + ": \"" + polly.getTierstimme() + "\"");
		System.out.println("Haustiere von Heinz: " + heinz.getHaustierbezeichnungen());
		
		Haustierhalter justus = new Haustierhalter(); // http://fragezeichen.neuvertonung.de/
		Papagei blackbeard = new Papagei("Blackbeard", 42);
		justus.neuesHaustier(blackbeard);
		System.out.println(blackbeard.getName() + ": \"" + blackbeard.getTierstimme() + "\"");
		
		Haustierhalter thrawn = new Haustierhalter(); // http://starwars.neuvertonung.de/
		for (int i = 0; i < 1000; i++) {
			thrawn.neuesHaustier(new Ysalamiri((i+""), (int) (Math.random()*100)));
		}
		System.out.println("Haustiere von Großadmiral Thrawn: " + thrawn.getHaustierbezeichnungen());
		
		Katze sammy = new Katze(/*Achtung der folgende Name ist ein Spoiler aus der ersten Staffel von Warrior Cats*/"Feuerstern", 10);
		//Da sich Sammy den, in der Wildnis lebenden, Clan-Katzen angeschlossen hat, hat Sammy keinen Haustierhalter mehr.
		
		Fisch strongFish91 = new Fisch("Undyne", 25);
		System.out.println(strongFish91.getName() + ": \"" + strongFish91.getTierstimme() + "\"");
	}
}

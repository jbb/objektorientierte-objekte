
public class Papagei implements Haustier {
	private String name;
	private int alter;
	
	public Papagei(String name, int alter) {
		this.name = name;
		this.alter = alter;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAlter() {
		return alter;
	}
	
	public String getBezeichnung() {
		return "Papagei";
	}
	
	public String getTierstimme() {
		if (name == "Blackbeard") {
			return "Ich bin Blackbeard der Pirat. Meinen Schatz vergrub ich in finsterer Nacht. Wo die Toten halten ewig Wacht. Johoo. Und ne Buddel voll Rum.";
		} else return name + ", möchte einen Keks!";
	}
}

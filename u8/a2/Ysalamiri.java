
public class Ysalamiri implements Haustier {
	private String name;
	private int alter;
	
	public Ysalamiri(String name, int alter) {
		this.name = name;
		this.alter = alter;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAlter() {
		return alter;
	}
	
	public String getBezeichnung() {
		return "Ysalamiri";
	}
	
	public String getTierstimme() {
		return "";
	}
}

#!/usr/bin/env python3
import math

def heron(a: float, b: float, c: float) -> float:
    s: float = (a + b + c) / 2
    return math.sqrt(s * (s - a) * (s - b) * (s - c))

print("Heron")
a = float(input("Enter a:"))
b = float(input("Enter b:"))
c = float(input("Enter c:"))
print(heron(a, b, c))

def ackermann_peter(n, m):
    if n == 0:
        return m + 1
    elif m == 0:
        return ackermann_peter(n - 1, 1)
    else:
        return ackermann_peter(n - 1, ackermann_peter(n, m - 1))

print("Ackermann-Peter Funktion")
n = float(input("Enter n:"))
m = float(input("Enter m:"))
print(ackermann_peter(n, m))

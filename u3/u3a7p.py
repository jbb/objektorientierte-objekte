
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("n", help="first number", type=int)
parser.add_argument("m", help="second number", type=int)
args = parser.parse_args()

n=args.n
m=args.m
# teilersumme von n bilden 
u=1
sn=0
while (2*u)<=n:
    if n%u==0:
        sn+=u
        u+=1
    else:
        u+=1
    

# teilersumme von mm bilden
u=1
sm=0
while (2*u)<=m:
    if m%u==0:
        sm+=u
        u+=1
    else:
        u+=1
        
# vergleichen
if n==sm:
    if m==sn:
        print("Die Zahlen", n, "und", m, "sind befreundet.")
    else:
        print("Die Zahlen", n, "und", m, "sind nicht befreundet.")
else:
    print("Die Zahlen", n, "und", m, "sind nicht befreundet.")
def produkt(liste) -> int:
    ergebnis = 1
    for zahl in liste:
        ergebnis = ergebnis * zahl
    return ergebnis

eingabe = input("[Aufgabe 6]: Bitte geben Sie eine durch Kommata getrennte Liste von Zahlen an: ")
eingabe = eingabe.replace("[", "")
eingabe = eingabe.replace("]", "")
zwischenspeicher = ""
zahlenListe = []
for buchstabe in eingabe:
    if (buchstabe == ','):
        zahlenListe.append(int(zwischenspeicher))
        zwischenspeicher = ""
    else: zwischenspeicher = zwischenspeicher + buchstabe
zahlenListe.append(int(zwischenspeicher))

print(produkt(zahlenListe))
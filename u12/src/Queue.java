import java.util.Iterator;

public interface Queue <E > {
    /* fuegt ein Element am Ende der Warteschlange ein */
    public void enqueue(E element);

    /*
     * das erste Element der Warteschlange wird entfernt und als Rueckgabewert der
     * Operation zurueckgegeben
     */
    public E dequeue() throws EmptyQueueException;

    /*
     * die Referenz des ersten Elements der Warteschlange wird zurueckgegeben ohne
     * diese zu entfernen
     */
    public E first() throws EmptyQueueException;

    /* ueberprueft , ob die Warteschlange leer ist */
    public boolean empty();

    /*
     * die Elemente der Warteschlange werden in die richtige Reihenfolge als
     * Zeichenkette zuruesckgegeben
     */
    public String toString();
}
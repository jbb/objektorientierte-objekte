public class TestArrayQueue {

    public static void main(String[] args) throws EmptyQueueException {
        LinkedQueue<String> q = new LinkedQueue<String>();
        q.enqueue("Friedrich");
        q.enqueue("Paul");
        q.enqueue("Jonah");
        System.out.println(q.toString());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }
}

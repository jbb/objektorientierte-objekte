import java.util.Iterator;

class LinkedQueue<T> implements Queue<T>, Iterable<T> {
    Node<T> head;
    Node<T> end;

    LinkedQueue() {
        head = new Node<T>(null);
        end = new Node<T>(null, head);
        head.setNext(end);
    }

    @Override
    public void enqueue (T element) {
        Node<T> node = new Node<T>(element);
        end.getPrev().setNext(node);
        node.setPrev(end.getPrev());
        node.setNext(end);
        end.setPrev(node);
    }

    @Override
    public T dequeue () throws EmptyQueueException {
        T value = first();
        head.setNext(head.getNext().getNext());
        return value;
    }

    @Override
    public T first () throws EmptyQueueException {
        if (empty()) {
            throw new EmptyQueueException();
        }
        return head.getNext().getValue();
    }

    @Override
    public boolean empty () {
        return head.getNext() == end;
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append("<");

        for (Node<T> current = head.getNext(); current != end; current = current.getNext()) {
            builder.append(current.getValue());
            builder.append(", ");
        }
        builder.append(">");

        return builder.toString();
    }

    class QueueIterator<T> implements Iterator<T> {
        Node<T> head;
        Node<T> end;
        Node<T> current;

        QueueIterator(Node<T> head, Node<T> end) {
            this.head = head;
            this.end = end;
            current = head;
        }

        @Override
        public boolean hasNext() {
            return current.getNext() != this.end;
        }

        @Override
        public T next() {
            current = current.getNext();
            return current.getValue();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new QueueIterator<T>(head, end);
    }
}
# Aufgabe 1

a) Iteratoren erlauben Zugriff auf ein Element in einer Sequenz. Sie können ihre Position auf der Sequenz ändern.
Dadurch erlauben sie eine robustere Art der Algorithmenprogrammierung im Vergleich zu integer Indices.
Iteratoren können in der Regel nicht auf nicht-existente Elemente zeigen, im Verlgeich zu Indices.

b) Abstrakte Datentypen sind das gegenteil von Primitiven Datentypen. Sie verbinden mehrere primitive Datentypen zu einem, in Programierspracchen meist als Klasse oder Struktur umgesetzt. Abstrakte Datentypen haben meist Attribute um die verschiedenen Einzelteile einzeln adressieren zu können.

c) Dynamische Datenstrukturen haben keien feste Speichergröße, sondern können dynamisch wachsen. Das wird zum Beispiel meist für Listen, Maps, etc. verwendet.

d) creators, producers, observers und mutators

e) Eine innere Klasse ist eine Klasse die in einer anderen Klasse definiert wird. Sie kann verwendet werden um Details der Klasse abzubilden, wie z.B. Iteratoren auf einer Listenklasse. Der Vorteil einer inneren Klasse besteht darin, dass sie nur von der außeren Klasse genutzt werden kann und dass die innere Klasse auf alle Attribute und Methoden der äußeren Klasse zugreifen kann.

f) Dummy-Knoten erlauben bei der Implementierung von verketteten Listen sicherzustellen dass jeder valide Knoten immer einen vorherigen und einen Nachfolgenden Knoten besitzen. Im Fall des ersten oder letzten Knoten ist dies dann ein Dummy-Knoten.
In der Implementierung erlauben Dummy-Knoten mit dem Anfang oder Ende der Liste zu vergleichen, um zu überprüfen ob es sich um den Knoten nach dem letzten handelt.

g) Doppelt verkettete Listen sind dann sinnvoll, wenn die Liste nicht nur in einer Richtung iteriert werden soll, also z.B. rückwärts, oder wenn oft auf Knoten am Ende der Liste zugegriffen werden soll ohne durch die gesamte Liste zu laufen.
In diesen Fällen können sie die algorithmische Komplexität reduzieren.

h) Bäume erlauben es bestimmte Operationen schneller als bei einer Liste durchführen zu können. Ein balancierter binärer Suchbaum zum Beispiel erlaubt es Suchen mit einer Laufzeitkomplexität von O(log(n)) durchzuführen.

# Aufgabe 2

```Java
public class EmptyQueueException extends Exception {
}

import java.util.Iterator;

class LinkedQueue<T> implements Queue<T>, Iterable<T> {
    Node<T> head;
    Node<T> end;

    LinkedQueue() {
        head = new Node<T>(null);
        end = new Node<T>(null, head);
        head.setNext(end);
    }

    @Override
    public void enqueue (T element) {
        Node<T> node = new Node<T>(element);
        end.getPrev().setNext(node);
        node.setPrev(end.getPrev());
        node.setNext(end);
        end.setPrev(node);
    }

    @Override
    public T dequeue () throws EmptyQueueException {
        T value = first();
        head.setNext(head.getNext().getNext());
        return value;
    }

    @Override
    public T first () throws EmptyQueueException {
        if (empty()) {
            throw new EmptyQueueException();
        }
        return head.getNext().getValue();
    }

    @Override
    public boolean empty () {
        return head.getNext() == end;
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append("<");

        for (Node<T> current = head.getNext(); current != end; current = current.getNext()) {
            builder.append(current.getValue());
            builder.append(", ");
        }
        builder.append(">");

        return builder.toString();
    }

    class QueueIterator<T> implements Iterator<T> {
        Node<T> head;
        Node<T> end;
        Node<T> current;

        QueueIterator(Node<T> head, Node<T> end) {
            this.head = head;
            this.end = end;
            current = head;
        }

        @Override
        public boolean hasNext() {
            return current.getNext() != this.end;
        }

        @Override
        public T next() {
            current = current.getNext();
            return current.getValue();
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new QueueIterator<T>(head, end);
    }
}
public class Node<T> {
    private Node<T> prev;
    private Node<T> next;
    private T value;

    public Node(T value) {
        this.value = value;
    }

    public Node(T value, Node<T> prev) {
        this.value = value;
        this.prev = prev;
    }

    public Node(T value, Node<T> prev, Node<T> next) {
        this.value = value;
        this.prev = prev;
        this.next = next;
    }

    public Node<T> getPrev() {
        return prev;
    }

    public void setPrev(Node<T> prev) {
        this.prev = prev;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}

import java.util.Iterator;

public interface Queue <E > {
    /* fuegt ein Element am Ende der Warteschlange ein */
    public void enqueue(E element);

    /*
     * das erste Element der Warteschlange wird entfernt und als Rueckgabewert der
     * Operation zurueckgegeben
     */
    public E dequeue() throws EmptyQueueException;

    /*
     * die Referenz des ersten Elements der Warteschlange wird zurueckgegeben ohne
     * diese zu entfernen
     */
    public E first() throws EmptyQueueException;

    /* ueberprueft , ob die Warteschlange leer ist */
    public boolean empty();

    /*
     * die Elemente der Warteschlange werden in die richtige Reihenfolge als
     * Zeichenkette zuruesckgegeben
     */
    public String toString();
}

public class TestArrayQueue {

    public static void main(String[] args) throws EmptyQueueException {
        LinkedQueue<String> q = new LinkedQueue<String>();
        q.enqueue("Friedrich");
        q.enqueue("Paul");
        q.enqueue("Jonah");
        System.out.println(q.toString());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }
}
```

package JavaGotchi;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class JavaGotchi {
	static enum Zustand {
		GLÜCKLICH, HUNGRIG, ESSEN, VERHUNGERT, NONE
	}
	static Zustand aktuellerZustand;
	static Zustand nächsterZustand = Zustand.NONE;
	static int hungerGrad;
	
	static void verhaltenHungrig() {
		System.out.println("Mir ist hungrig");
		System.out.printf("Ich verhungere in %d\n", 25 - hungerGrad);
		
		System.out.println("Gib mir Whiskas (Y/N)");
		Scanner scanner = new Scanner(System.in);
		String text = scanner.nextLine();
		if (text.startsWith("Y")) {
			nächsterZustand = Zustand.ESSEN;
		} else {
			hungerGrad++;
		}
	}
	
	static void verhaltenVerhungert() {
		System.out.println("I bims verhungert");
		System.out.println("Kann man nix machen");
	}
	
	static void verhaltenEssen() {
		System.out.println("Danke");
		System.out.println("Essen. So wichtig.");
		hungerGrad = 0;
	}
	
	static void verhaltenGlücklich() {
		System.out.println("Ich vibe gerade tootaaaal");
		hungerGrad++;
	}
	
	static void aktualisierung() {
		System.out.println("Ich analysiere ... ");
		
		if (nächsterZustand != Zustand.NONE) {
			aktuellerZustand = nächsterZustand;
			nächsterZustand = Zustand.NONE;
			return;
		}
		
		if (hungerGrad > 5) {
			aktuellerZustand = Zustand.HUNGRIG;
		}

		if (hungerGrad < 1) {
			aktuellerZustand = Zustand.GLÜCKLICH;
		}
		
		if (hungerGrad > 24) {
			aktuellerZustand = Zustand.VERHUNGERT;
		}
	}
	
	static void warte(int ms) {
		//System.out.println("Mach mal schneller");
		try {
			TimeUnit.MILLISECONDS.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static void starteDasSpiel() {
		System.out.println("Achtung: Das nachvolgende Programm kann memes enthalten.");
		aktuellerZustand = Zustand.GLÜCKLICH;
		hungerGrad = 0;
		boolean spielLaeuft = true;
		
		// Programmschleife
		while (spielLaeuft) {
			switch (aktuellerZustand) {
			case HUNGRIG:
				verhaltenHungrig();
				break;
			case VERHUNGERT:
				verhaltenVerhungert();
				spielLaeuft = false;
				break;
			case ESSEN:
				verhaltenEssen();
				break;
			case GLÜCKLICH:
				verhaltenGlücklich();
				break;
			case NONE:
				
			}
			
			aktualisierung();
			warte(400);
		}
	}
}

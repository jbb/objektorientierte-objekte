public class U5 {
	
	public static void main(String[] args) {
		System.out.println(wallisProdukt(100000000) + " = " + Math.PI/2);
		System.out.println(newton(100000000) + " = " + Math.PI/(2.0*Math.sqrt(2)));
		System.out.println(euler(100000000) + " = " + Math.PI/(3.0*Math.sqrt(3)));
	}
	
	public static double wallisProdukt(double n) {
		double ergebnis = 1.0;
		for (int i = 1; i <= n; i++) {
			int zaehler, nenner;
			if (i%2 == 0) {
				zaehler = i;
				nenner = i+1;
			} else {
				zaehler = i+1;
				nenner = i;
			}
			ergebnis *= ((double) zaehler) / ((double) nenner);
		}
		return ergebnis;
	}
	
	public static double newton(double n) {
		double ergebnis = 1.0;
		for (int i = 1; i <= n; i++) {
			int zaehler = 1;
			int nenner = 2*i+1;
			if ((i/2)%2==0) {
				ergebnis += ((double) zaehler) / ((double) nenner);
			} else {
				ergebnis -= ((double) zaehler) / ((double) nenner);
			}
		}
		return ergebnis;
	}
	
	public static double euler(double n) {
		double ergebnis = 1.0;
		int nenner = 1;
		for (int i = 1; i <= n; i++) {
			int zaehler = 1;
			if (i%2==0) {
				nenner += 2;
				ergebnis += ((double) zaehler) / ((double) nenner);
			} else {
				nenner += 1;
				ergebnis -= ((double) zaehler) / ((double) nenner);
			}
		}
		return ergebnis;
	}
}
